x <- 2
x


class(x)

is.numeric(x)


y <- 6L
y
class(y)
is.numeric(y)
is.integer(y)

5L / 2L
class(5L/2L)


thestring <- "data"



z <- 2
z























class(thestring)

thestring <- factor("Ammer")
thestring


nchar(thestring)




date1 <- as.Date("2016-03-06")
date1

class(date1)
as.numeric(date1)






date2 <-  as.POSIXct("2016-03-06 20:17:00")
date2

class(date2)
as.numeric(date2)

k <- TRUE
k

class(k)
is.logical(k)


2==3
2==2
class(2==2)

2!=3


"Ammer" == "Ammer"






x <- c(1,2,3,4)
x

y <- x * 3
y

sqrt(y)

1:2

x <- 1:10
y <- -5:4
x
y

x+y
x - y

x/y
X^Y
x^y

length(x)
length(y)

length(x+y)


x + c(1,2)


x + c(1,2,3)

X <= 5
x > y

any(x<y)
all(x<y)


q <-  c(First = "Hockey", Second = "Football", Third = "cricket" , Fourth =  "Baseball")
q

nchar(q)


q[1:2]
q[3]
q[4]
q[c(1,4)]


q <- c(First = "Hockey")
q
q <-  c(First = "Hockey", Second = "Football", Third = "cricket" , Fourth =  "Baseball")
q



c(one = "a" , Two = "b" , Three ="C")
w <- 1:3
w
names(w) <- c("a" , "b" , "c")
w

q2 <- c(q, fifth = "pikachu" , sixth = "Hockey")
q2



q2factor <- factor(q2)
q2factor

as.numeric(q2factor)




z <- c(1,2,NA,3)
z

is.na(z)

z <- c(1,NULL,3)
z
is.null(z)



x <- 34:45
x
sum(x)
mean(x)
nchar(x)


mean(x[1:5])
sum(x[1:5])



y <- c(5,6,7,8)
mean(y)

mean(x=y,trim = 0.1)

















require(ggplot2)
data(diamonds)
head(diamonds)





hist(diamonds$carat)
head(economics)


ggplot2






w <- 1:3
w

rm(w)
w
w <- c(First = 1 , Second =2 , Third = 3)
w





"---------Data Frame------"

X <- 1:10
y <- -5:15
X
y
rm(y)

y <- -4:5
y


q <- c("Hockey" , "Football" , "Volleyball", "Cricket", "Soccer", "Rugby", "lacrosse", "Curling")
q

z <- c(q, "Hockey", "Skiewing")
z

x <- 1:10
x



theDF = data.frame(x, y, z)
theDF

theDF = data.frame(First=x, Second=y, Third=z, stringsAsFactors = TRUE)
theDF


class(theDF$Third)



nrow(theDF)
ncol(theDF)

length(theDF)



names(theDF)

names(theDF)[3]

rownames(theDF)

rownames(theDF) <- c("Frst", "Second", "Three", "four", "five","six", "seven","eight","nine","ten")
theDF

rownames(theDF) <- NULL
theDF




head(theDF, n=7)


tail(theDF)


class(theDF)



theDF$First

z <- theDF[3,2]
z


z <- theDF[3,1:3]
z

theDF[c(3,5), 1:3]
theDF[ , 1:3]
theDF[ , 1:2]

theDF[2,]


theDF[, c("Third", "FIrst")]
theDF
theDF[, "Third"]
theDF[, c("Third", "Second") , drop = FALSE]










"---------------List-------------"

list1 <- list(1,2,3)
list1


list2 <- list(c(1,2,3))
list2


emptylist <- vector(mode = "list", length = 4)
emptylist


emptylist[[1]] <- 5
emptylist

list5



"--------------Matrix-----------------"

A <- matrix(1:10 , nrow = 5)
A
B <- matrix(21:30 , nrow = 5)
B
C <- matrix(21:40, nrow = 2)
c


A
B
C



A + B

A * B


A == B


nrow(A)
ncol(B)
t(B)


A %*% t(B)


ncol(A)
nrow(C)
A %*% C


colnames(A)
rownames(A)


colnames(A) <- c("Left", "right")
rownames(A) <- c("first", "Second", "Third", "Fourth", "Fifth")

A


LETTERS
colnames(C) <- LETTERS[1:10]
C
rownames(C) <- c("First", "Second")
C

A %*% C






"-----------------Arrays---------------"

theArray <- array(1:12 , dim = c(2, 6, 3))
theArray



theArray[1, ,]
theArray[1, ,1]
theArray[2, ,1]
theArray[1, ,2]
theArray[,1 ,1]
theArray

theList <- List(A <- c(1:2) , B <- c(3:4))
theList


theList

theList

